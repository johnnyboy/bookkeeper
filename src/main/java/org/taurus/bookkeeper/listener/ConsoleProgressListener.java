package org.taurus.bookkeeper.listener;

public class ConsoleProgressListener implements ProgressListener {

	@Override
	public void onProgressChanged(ProgressChangedEvent evt) {
		String status = String.format("[%d/%d]", evt.processed(),
				evt.total());
		System.out.printf("%15s - %s%n", status, evt.percentage());
	}
}