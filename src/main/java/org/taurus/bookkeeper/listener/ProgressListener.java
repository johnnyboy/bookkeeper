package org.taurus.bookkeeper.listener;

public interface ProgressListener {

	void onProgressChanged(ProgressChangedEvent pce);
}