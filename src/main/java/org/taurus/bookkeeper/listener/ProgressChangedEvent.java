package org.taurus.bookkeeper.listener;

public class ProgressChangedEvent {
	private final int total;
	private final int processed;

	public ProgressChangedEvent(int totalBytes, int bytesProcessed) {
		this.total = totalBytes;
		this.processed = bytesProcessed;
	}

	public int total() {
		return total;
	}

	public int processed() {
		return processed;
	}

	public String percentage() {
		Double percentage = (processed() * 1D / total()) * 100;
		return String.valueOf(percentage.intValue()).concat("%");
	}

}