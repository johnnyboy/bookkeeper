package org.taurus.bookkeeper.command;

import java.net.URL;

import org.kohsuke.args4j.Option;

public class BookCommand {

	@Option(name = "-f", aliases = "--from", metaVar = "url", usage = "Online library book URL", required = true)
	public URL baseUrl;

	@Option(name = "-t", aliases = "--to", metaVar = "file", usage = "File name", required = true)
	public String name;

}