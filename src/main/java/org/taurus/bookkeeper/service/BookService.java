package org.taurus.bookkeeper.service;

import org.taurus.bookkeeper.controller.DelayController;
import org.taurus.bookkeeper.controller.IoController;
import org.taurus.bookkeeper.controller.NetworkController;
import org.taurus.bookkeeper.listener.ProgressChangedEvent;
import org.taurus.bookkeeper.listener.ProgressListener;
import org.taurus.bookkeeper.model.BookInfo;

import java.nio.file.Path;

public class BookService {
	private final ProgressListener progressListener;
	private final NetworkController networkController;
	private final IoController ioController;
	private final DelayController delayController;

	public BookService(NetworkController networkController, IoController ioController, DelayController delayController, ProgressListener progressListener) {
		this.networkController = networkController;
		this.ioController = ioController;
		this.delayController = delayController;
		this.progressListener = progressListener;
	}

	public Path save(String title) {
		return ioController.save(title, loadContent());
	}

	private StringBuilder loadContent() {
		BookInfo bookInfo = networkController.bookInfo();
		StringBuilder sb = new StringBuilder(bookInfo.getByteCount());
		for (int page = 1; page <= bookInfo.getPageCount(); page++) {
			delayController.delay();
			sb.append(networkController.chunk(page));
			fireProgressChange(page, bookInfo);
		}
		return sb;
	}

	private void fireProgressChange(int page, BookInfo bookInfo) {
		ProgressChangedEvent progressChangedEvent = new ProgressChangedEvent(bookInfo
				.getByteCount(), page * BookInfo.CHUNK_SIZE);
		progressListener.onProgressChanged(progressChangedEvent);
	}

}