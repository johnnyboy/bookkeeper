package org.taurus.bookkeeper.model;

public class BookInfo {
	public static final int CHUNK_SIZE = 20480;
	private final String id;
	private final int pageCount;
	private final int byteCount;

	public BookInfo(String id, int pageCount) {
		this.id = id;
		this.pageCount = pageCount;
		this.byteCount = pageCount * CHUNK_SIZE;
	}

	public String getId() {
		return id;
	}

	public int getPageCount() {
		return pageCount;
	}

	public int getByteCount() {
		return byteCount;
	}

}