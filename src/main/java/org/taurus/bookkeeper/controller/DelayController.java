package org.taurus.bookkeeper.controller;

import java.util.Random;
import java.util.concurrent.TimeUnit;

public class DelayController {
	private final Random random = new Random();
	private final int maxDelayInSeconds;

	class Range {
		final int lo;
		final int hi;

		Range(int lo, int hi) {
			this.lo = lo;
			this.hi = hi;
		}
	}

	public DelayController(int maxDelayInSeconds) {
		this.maxDelayInSeconds = maxDelayInSeconds;
	}

	public void delay() {
		try {
			Range range = millisRange();
			TimeUnit.MILLISECONDS.sleep(range.lo + random.nextInt(range.hi));
		} catch (InterruptedException e) {
			throw new RuntimeException(e.getMessage(), e);
		}
	}

	Range millisRange() {
		return new Range(1, maxDelayInSeconds * 1000);
	}
}