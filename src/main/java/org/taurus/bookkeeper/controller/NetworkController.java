package org.taurus.bookkeeper.controller;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.taurus.bookkeeper.model.BookInfo;
import org.taurus.bookkeeper.parser.DocumentParser;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class NetworkController {
	private final int timeout = (int) TimeUnit.SECONDS.toMillis(5);
	private final DocumentParser documentParser;
	private final URL baseUrl;
	private final BookInfo bookInfo;

	public NetworkController(DocumentParser documentParser, URL baseUrl) {
		this.documentParser = documentParser;
		this.baseUrl = baseUrl;
		this.bookInfo = loadBookInfo(baseUrl);
	}

	private BookInfo loadBookInfo(URL url) {
		try {
			@SuppressWarnings("deprecation")
			Document doc = Jsoup.connect(url.toString()).validateTLSCertificates(false).timeout(timeout)
					.get();
			String bookId = doc.getElementById("bid").text();
			Element pageSelect = doc.getElementById("topSearchDiv").selectFirst("center").selectFirst("select");
			int pageCount = pageSelect.getElementsByTag("option").size();
			return new BookInfo(bookId, pageCount);
		} catch (IOException e) {
			throw new IllegalStateException(e.getMessage(), e);
		}
	}

	public BookInfo bookInfo() {
		return bookInfo;
	}

	public String chunk(int page) {
		try {
			@SuppressWarnings("deprecation")
			Document doc = Jsoup.connect(resolveChunkUrl().toString()).validateTLSCertificates(false)
					.timeout(timeout).data(queryParams(page)).get();
			return documentParser.parse(doc);
		} catch (IOException e) {
			throw new IllegalStateException(e.getMessage(), e);
		}
	}

	private URL resolveChunkUrl() {
		try {
			return new URL(baseUrl.getProtocol(), baseUrl.getHost(),
					baseUrl.getPort(), "/br.php");
		} catch (MalformedURLException e) {
			throw new IllegalArgumentException(e.getMessage(), e);
		}
	}

	private Map<String, String> queryParams(int page) {
		Map<String, String> queryParams = new HashMap<>();
		queryParams.put("i", bookInfo.getId());
		queryParams.put("pos", String.valueOf((page - 1) * BookInfo.CHUNK_SIZE));
		queryParams.put("rnd", String.valueOf(Math.random()));
		return queryParams;
	}

}