package org.taurus.bookkeeper.controller;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

public class IoController {
	private final Path basePath;

	public IoController() {
		try {
			this.basePath = Paths.get(System.getProperty("user.home"),
					"bookkeeper");
			Files.createDirectories(basePath);
		} catch (IOException e) {
			throw new IllegalStateException(e.getMessage(), e);
		}
	}

	public Path getBasePath() {
		return basePath;
	}

	public Path save(String title, StringBuilder content) {
		String fileName = String.format("%s.txt", title);
		Path bookPath = basePath.resolve(fileName);
		try (BufferedWriter writer = Files.newBufferedWriter(bookPath,
				StandardCharsets.UTF_8, StandardOpenOption.CREATE_NEW,
				StandardOpenOption.WRITE)) {
			writer.write(content.toString());
		} catch (IOException e) {
			throw new IllegalStateException(e.getMessage(), e);
		}
		return bookPath;
	}
}