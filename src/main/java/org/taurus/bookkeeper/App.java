package org.taurus.bookkeeper;

import org.kohsuke.args4j.CmdLineException;
import org.taurus.bookkeeper.command.BookCommand;
import org.taurus.bookkeeper.controller.DelayController;
import org.taurus.bookkeeper.controller.IoController;
import org.taurus.bookkeeper.controller.NetworkController;
import org.taurus.bookkeeper.listener.ConsoleProgressListener;
import org.taurus.bookkeeper.parser.CommandLineParser;
import org.taurus.bookkeeper.parser.DocumentParser;
import org.taurus.bookkeeper.service.BookService;

import java.nio.file.Path;

public class App {

	private void process(BookCommand book) {
		NetworkController networkController = new NetworkController(new DocumentParser(), book.baseUrl);
		IoController ioController = new IoController();
		ConsoleProgressListener progressListener = new ConsoleProgressListener();
		DelayController delayController = new DelayController(2);

		BookService bookService = new BookService(networkController, ioController, delayController, progressListener);

		System.out.printf("Processing %s%n", book.name);
		Path path = bookService.save(book.name);
		System.out.printf("Saved %s%n", path.toAbsolutePath()
				.toString());
	}

	public static void main(String[] args) {
		CommandLineParser parser = new CommandLineParser();
		try {
			BookCommand book = parser.parse(args);
			new App().process(book);
		} catch (CmdLineException e) {
			System.out.println(e.getMessage());
			parser.printUsage(System.out);
		} catch (RuntimeException e) {
			System.out.println("Fatal error occurred, app will now shut down");
			e.printStackTrace(System.out);
		}
	}

}