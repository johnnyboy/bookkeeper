package org.taurus.bookkeeper.parser;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.parser.Parser;
import org.jsoup.safety.Whitelist;

public class DocumentParser {

	private final Document.OutputSettings noPrettyPrint = new Document.OutputSettings().prettyPrint(false);

	public String parse(Document document) {
		String text = stripHtmlPreserveLineBreaks(document);
		return unescapeHtmlEntities(text);
	}

	private String stripHtmlPreserveLineBreaks(Document doc) {
		return Jsoup.clean(doc.html(), doc.baseUri(), Whitelist.none(), noPrettyPrint);
	}

	private String unescapeHtmlEntities(String text) {
		return Parser.unescapeEntities(text, false);
	}
}