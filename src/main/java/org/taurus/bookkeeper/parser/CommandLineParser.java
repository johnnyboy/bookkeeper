package org.taurus.bookkeeper.parser;

import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.taurus.bookkeeper.command.BookCommand;

import java.io.PrintStream;

public class CommandLineParser {
	private final CmdLineParser parser;
	private final BookCommand book;

	public CommandLineParser() {
		book = new BookCommand();
		parser = new CmdLineParser(book);
	}

	public BookCommand parse(String[] args) throws CmdLineException {
		parser.parseArgument(args);
		return book;
	}

	public void printUsage(PrintStream stream) {
		parser.printUsage(stream);
	}
}
