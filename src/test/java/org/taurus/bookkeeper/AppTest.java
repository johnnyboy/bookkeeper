package org.taurus.bookkeeper;

import org.junit.Rule;
import org.junit.Test;
import org.taurus.bookkeeper.rule.SystemOutCapturingRule;

import static org.junit.Assert.assertTrue;

public class AppTest {

	@Rule
	public final SystemOutCapturingRule sysOut = new SystemOutCapturingRule();

	@Test
	public void invalidArgsCauseUsagePrint() {
		//given
		String[] args = new String[]{};

		//when
		App.main(args);

		//then
		assertTrue(sysOut.content().contains("-f (--from) url : Online library book URL"));
		assertTrue(sysOut.content().contains("-t (--to) file  : File name"));
	}
}
