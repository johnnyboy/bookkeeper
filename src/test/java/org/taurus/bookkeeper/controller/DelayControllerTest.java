package org.taurus.bookkeeper.controller;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class DelayControllerTest {

	@Test
	public void durationFitsBounds() {
		//given
		int maxDelayInSeconds = 2;
		DelayController delayController = new DelayController(maxDelayInSeconds);

		//when
		DelayController.Range range = delayController.millisRange();

		//then
		assertTrue(range.lo == 1);
		assertTrue(range.hi == maxDelayInSeconds * 1000);
	}
}
