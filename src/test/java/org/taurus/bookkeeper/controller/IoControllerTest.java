package org.taurus.bookkeeper.controller;

import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class IoControllerTest {


	@Test
	public void basePathExistsAsReadableAndWritableDirectory() {
		//given
		IoController ioController = new IoController();

		//when
		Path basePath = ioController.getBasePath();

		//then
		assertNotNull(basePath);
		assertTrue(Files.exists(basePath));
		assertTrue(Files.isDirectory(basePath));
		assertTrue(Files.isReadable(basePath));
		assertTrue(Files.isWritable(basePath));
	}

	@Test
	public void saveWritesContentToFile() throws IOException {
		//given
		IoController ioController = new IoController();
		String title = "testFileName";
		StringBuilder content = new StringBuilder("Hello bookkeeper");

		//when
		Path path = ioController.save(title, content);

		//then
		assertNotNull(path);
		assertTrue(Files.exists(path));
		assertTrue(Files.size(path) > 0);
		assertTrue(path.getFileName().toString().startsWith(title));
		List<String> lines = Files.readAllLines(path);
		assertNotNull(lines);
		assertTrue(lines.size() == 1);
		String fileContent = lines.iterator().next();
		assertEquals(content.toString(), fileContent);

		Files.delete(path);
	}
}
