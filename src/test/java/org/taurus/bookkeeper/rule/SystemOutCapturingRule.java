package org.taurus.bookkeeper.rule;

import org.junit.rules.ExternalResource;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class SystemOutCapturingRule extends ExternalResource {
	private PrintStream sysOut;

	private final ByteArrayOutputStream capturingStream = new ByteArrayOutputStream();

	@Override
	protected void before() {
		sysOut = System.out;
		System.setOut(new PrintStream(capturingStream));
	}

	@Override
	protected void after() {
		System.setOut(sysOut);
	}

	public String content() {
		return capturingStream.toString();
	}
}
