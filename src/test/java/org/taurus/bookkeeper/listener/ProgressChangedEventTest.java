package org.taurus.bookkeeper.listener;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class ProgressChangedEventTest {

	private final int total;
	private final int processed;
	private final String expected;

	public ProgressChangedEventTest(int total, int processed, String expected) {
		this.total = total;
		this.processed = processed;
		this.expected = expected;
	}

	@Parameters
	public static Collection<Object[]> data() {
		return Arrays.asList(new Object[][]{
				{100, 20, "20%"},
				{100, 37, "37%"},
				{100, 0, "0%"},
				{100, 100, "100%"}
		});
	}

	@Test
	public void percentage() {
		//given
		ProgressChangedEvent evt = new ProgressChangedEvent(total, processed);

		//when
		String percentage = evt.percentage();

		//then
		Assert.assertEquals(expected, percentage);
	}
}
