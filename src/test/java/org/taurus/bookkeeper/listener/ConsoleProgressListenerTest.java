package org.taurus.bookkeeper.listener;

import org.junit.Rule;
import org.junit.Test;
import org.taurus.bookkeeper.rule.SystemOutCapturingRule;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ConsoleProgressListenerTest {

	@Rule
	public final SystemOutCapturingRule sysOut = new SystemOutCapturingRule();

	@Test
	public void onProgressChangedReadsTotalProcessedAndPercentage() {
		//given
		ProgressChangedEvent evt = mock(ProgressChangedEvent.class);
		when(evt.total()).thenReturn(100);
		when(evt.processed()).thenReturn(20);
		when(evt.percentage()).thenReturn("20%");
		ConsoleProgressListener listener = new ConsoleProgressListener();

		//when
		listener.onProgressChanged(evt);

		//then
		verify(evt).total();
		verify(evt).processed();
		verify(evt).percentage();
		assertTrue(sysOut.content().contains("[20/100] - 20%"));
	}
}
