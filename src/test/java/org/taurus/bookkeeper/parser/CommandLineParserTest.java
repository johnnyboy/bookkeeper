package org.taurus.bookkeeper.parser;

import org.junit.Test;
import org.kohsuke.args4j.CmdLineException;
import org.taurus.bookkeeper.command.BookCommand;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class CommandLineParserTest {

	@Test
	public void fromArgumentIsRequired() {
		//given
		CommandLineParser parser = new CommandLineParser();
		String[] args = new String[]{"--to", "baz"};

		//when
		try {
			parser.parse(args);
			fail("Will not happen");
		} catch (CmdLineException e) {
			assertEquals("Option \"-f (--from)\" is required", e.getMessage());
		}
	}

	@Test
	public void toArgumentIsRequired() {
		//given
		CommandLineParser parser = new CommandLineParser();
		String[] args = new String[]{"--from", "https://www.google.com"};

		//when
		try {
			parser.parse(args);
			fail("Will not happen");
		} catch (CmdLineException e) {
			assertEquals("Option \"-t (--to)\" is required", e.getMessage());
		}
	}

	@Test
	public void fromArgumentHasToBeValidUrl() {
		//given
		CommandLineParser parser = new CommandLineParser();
		String[] args = new String[]{"--from", "google.com"};

		//when
		try {
			parser.parse(args);
			fail("Will not happen");
		} catch (CmdLineException e) {
			assertEquals("\"google.com\" is not a valid value for \"--from\"", e.getMessage());
		}
	}

	@Test
	public void unknownArgumentIsRejected() {
		//given
		CommandLineParser parser = new CommandLineParser();
		String[] args = new String[]{"--from", "https://google.com", "--to", "baz", "--foo", "bar"};

		//when
		try {
			parser.parse(args);
			fail("Will not happen");
		} catch (CmdLineException e) {
			assertEquals("\"--foo\" is not a valid option", e.getMessage());
		}
	}

	@Test
	public void correctArgumentsAreBoundToCommand() {
		//given
		CommandLineParser parser = new CommandLineParser();
		String[] args = new String[]{"--from", "https://google.com", "--to", "title"};
		BookCommand book = null;

		//when
		try {
			book = parser.parse(args);
		} catch (CmdLineException e) {
			fail("Will not happen");
		}

		//then
		assertEquals("https://google.com", book.baseUrl.toString());
		assertEquals("title", book.name);
	}

}
