package org.taurus.bookkeeper.parser;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class DocumentParserTest {

	@Test
	public void parseCleansHtmlTagsPreservesLineBreaksAndUnescapesHtmlEntities() {
		//given
		Document doc = Jsoup.parse(raw());
		DocumentParser parser = new DocumentParser();

		//when
		String parsed = parser.parse(doc);

		//then
		assertEquals(expected(), parsed);
	}

	private String raw() {
		return "<html>" +
				"<head></head>" +
				"<body>" +
				"Old Mc&apos;Donald had a farm<br/>" +
				"E-I-E-I-O" +
				"</body>" +
				"</html>";
	}

	private String expected() {
		return String.format("%n %n %n  Old Mc'Donald had a farm%n  E-I-E-I-O%n %n");
	}
}
