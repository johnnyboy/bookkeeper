package org.taurus.bookkeeper.service;

import org.junit.Test;
import org.taurus.bookkeeper.controller.DelayController;
import org.taurus.bookkeeper.controller.IoController;
import org.taurus.bookkeeper.controller.NetworkController;
import org.taurus.bookkeeper.listener.ConsoleProgressListener;
import org.taurus.bookkeeper.listener.ProgressChangedEvent;
import org.taurus.bookkeeper.listener.ProgressListener;
import org.taurus.bookkeeper.model.BookInfo;

import java.nio.file.Path;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class BookServiceTest {

	@Test
	public void bookServiceLoadsBookInfoOnceLoadsChunkAndNotifiesProgressListenerPerPage() {
		//given
		BookInfo bookInfo = mock(BookInfo.class);
		Path path = mock(Path.class);
		NetworkController networkController = mock(NetworkController.class);
		IoController ioController = mock(IoController.class);
		ProgressListener progressListener = mock(ConsoleProgressListener.class);
		DelayController delayController = mock(DelayController.class);

		int pageCount = 3;
		when(bookInfo.getPageCount()).thenReturn(pageCount);
		when(networkController.bookInfo()).thenReturn(bookInfo);
		when(networkController.chunk(anyInt())).thenReturn("chunk");
		doNothing().when(delayController).delay();
		doNothing().when(progressListener).onProgressChanged(any(ProgressChangedEvent.class));
		when(ioController.save(anyString(), any(StringBuilder.class))).thenReturn(path);

		//when
		BookService bookService = new BookService(networkController, ioController, delayController, progressListener);
		bookService.save("Title");

		//then
		verify(networkController, times(1)).bookInfo();
		verify(networkController, times(pageCount)).chunk(anyInt());
		verify(delayController, times(pageCount)).delay();
		verify(progressListener, times(pageCount)).onProgressChanged(any(ProgressChangedEvent.class));
	}
}
